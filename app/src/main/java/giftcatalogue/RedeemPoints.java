package giftcatalogue;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.GiftRedeemedstatus;
import com.brainmagic.lucasindianservice.demo.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ApiInterface.CategoryAPI;
import adapter.GiftPreviewAdapter;
import adapter.GiftStatusAdapter;
import adapter.GiftsHistoryAdapter;
import adapter.RedeemGiftAdapter;
import alertbox.Alertbox;
import api.models.gifthistory.GetGiftHistory;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import home.Home_Activity;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import model.giftcatalogue.jodidargiftresponse.JodhiaretalierData;
import model.giftcatalogue.jodidargiftresponse.JodidarandretailerGiftSelectedResult;
import model.giftcatalogue.redeempoints.JodidarRedeemPoints;
import model.giftcatalogue.redeempoints.Redeem;
import model.giftsumma.GiftCode;
import model.giftsumma.SendSelectedGifts;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class RedeemPoints extends AppCompatActivity implements RedeemGiftAdapter.ClickInterface {

    private TextView selectedPoints;
    private static final String TAG = "RedeemPoints";
    public  JodidarRedeemPoints pointsLists;
    private Disposable disposable;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Button redeem_submit;
    private List<GiftCode> giftCodeList;
    private List<String> gcodes;
    private List<String> gnames;
    private List<String> gpointss;
    private Alertbox alertbox = new Alertbox(this);
    private List<JodhiaretalierData> statusgiftredemmed;
//    private List<Redeem> rdeemdataget;
//    private GiftPreviewAdapter giftPreviewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_points);

        ImageView back=findViewById(R.id.redeem_back);
        ImageView home=findViewById(R.id.redeem_home);
        ListView list=findViewById(R.id.redeem_list_details);
        redeem_submit=findViewById(R.id.redeem_submit);

        statusgiftredemmed=new ArrayList<>();
        selectedPoints=findViewById(R.id.selected_points);
        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        editor = myshare.edit();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RedeemPoints.this,Home_Activity.class)
                .addFlags(FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK ));
            }
        });

        NetworkConnection networkConnection=new NetworkConnection(RedeemPoints.this);
        if(networkConnection.CheckInternet()){
            getRxJavaRedeemPointsResponse();
        }
        else {
            StyleableToasty styleableToasty=new StyleableToasty(RedeemPoints.this);
            styleableToasty.showSuccessToast("No Internet Connection. Please try again later");
        }



//        List<String> gCode=new ArrayList<>();
//        gCode.add("Mobile");
//        gCode.add("Pen");
//        gCode.add("Watch");
//        gCode.add("Bottle");
//        gCode.add("Pc");
//
//        List<Integer> points=new ArrayList<>();
//        points.add(20);
//        points.add(30);
//        points.add(50);
//        points.add(75);
//        points.add(100);


//        RedeemGiftAdapter redeemGiftAdapter=new RedeemGiftAdapter(RedeemPoints.this,gCode,points,100,80);
//        list.setAdapter(redeemGiftAdapter);
    }


    @Override
    public void onCheckedListener(List<GiftCode> list, int points, boolean isChecked,List<String> gcode,List<String> gname,List<String> gpoints) {
//        Toast.makeText(getApplicationContext(),""+list,Toast.LENGTH_SHORT).show();
        this.giftCodeList=list;
        this.gcodes=gcode;
        this.gnames=gname;
        this.gpointss=gpoints;
        selectedPoints.setText(points+"");
    }

    private void getRxJavaRedeemPointsResponse()
    {

        final ProgressDialog loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
        String userType=getIntent().getStringExtra("UserType");
        String userCode=getIntent().getStringExtra("UserCode");
        String mobileNo=getIntent().getStringExtra("MobileNo");

        APIService service = RetroClient.getSapGiftPRetrofitService();
        Observable<JodidarRedeemPoints> observable = null;
        if(userType.equals("Jodidar"))
            observable=service.jodidarRedeemPoints(mobileNo,userCode);
        else
            observable=service.retailerRedeemPoints(mobileNo,userCode);

        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<JodidarRedeemPoints>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "onSubscribe: ");
                    }

                    @Override
                    public void onNext(JodidarRedeemPoints pointsList) {
                        if(pointsList.getResult().equals("Success"))
                        {
                           pointsLists=pointsList;
                        }
                        else {
                            Alertbox alertbox=new Alertbox(RedeemPoints.this);
                            alertbox.showNegativebox("");
                        }
//                        RedeemGiftAdapter redeemGiftAdapter=new RedeemGiftAdapter(RedeemPoints.this,gCode,points,100,80);
//                        list.setAdapter(redeemGiftAdapter);
                        Log.d(TAG, "onNext: ");
                    }
                    @Override
                    public void onError(Throwable e) {
                        loading.dismiss();
                        Log.d(TAG, "onError: "+e.getMessage());
                    }
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete: ");

                        ListView list=findViewById(R.id.redeem_list_details);
                        TextView totalPoint=findViewById(R.id.total_redeem_points);
                        TextView redeemablePoint=findViewById(R.id.redeemable_points);

                        if(pointsLists!=null) {
                            if (pointsLists.getData().getRedeem().size() != 0) {
                                String totalPoints = pointsLists.getData().getRecData().get(0).getPoints();
                                int tempPoints = Integer.parseInt(pointsLists.getData().getRecData().get(0).getPoints().trim());
                                int redeemablePoints = (tempPoints * 80) / 100;
                                totalPoint.setText(totalPoints);
                                redeemablePoint.setText(redeemablePoints + "");
                                loading.dismiss();
                                RedeemGiftAdapter redeemGiftAdapter = new RedeemGiftAdapter(RedeemPoints.this, pointsLists, totalPoints, redeemablePoints);
                                list.setAdapter(redeemGiftAdapter);
                            }
                            else {
                                loading.dismiss();
                                Alertbox alertbox=new Alertbox(RedeemPoints.this);
                                alertbox.showNegativebox("No Points  Found");
                            }
                        }
                        else {
                            loading.dismiss();
                            Alertbox alertbox=new Alertbox(RedeemPoints.this);
                            alertbox.showNegativebox("No Points Found");
                        }
                    }
                });

        redeem_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (giftCodeList==null  || giftCodeList.size()==0) {

                    alertbox.showAlertbox("Please Select Gift");
                }
                else {

                    getgiftselected(gcodes,gnames,gpointss);
                }
            }
            private void getgiftselected(List<String> gcodes, List<String> gnames, List<String> gpointss) {

                final AlertDialog alertDialogpreview;
                alertDialogpreview = new AlertDialog.Builder(RedeemPoints.this).create();
                LayoutInflater inflater = (RedeemPoints.this).getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.reedem_gift_selected, null);
                alertDialogpreview.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
                alertDialogpreview.setView(dialogView);
                alertDialogpreview.setCanceledOnTouchOutside(false);
                ListView giftselected=dialogView.findViewById(R.id.giftselected);
                Button okgift=dialogView.findViewById(R.id.okgift);

                giftselected.setAdapter(new GiftPreviewAdapter(RedeemPoints.this,gcodes,gnames,gpointss));

                okgift.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final ProgressDialog progressDialog = new ProgressDialog(RedeemPoints.this,
                                R.style.Progress);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setMessage("Loading...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();

                        SendSelectedGifts sendSelectedGifts = new SendSelectedGifts();

                        sendSelectedGifts.setGiftCode(giftCodeList);


                        String userType = getIntent().getStringExtra("UserType");
                        String userCode = getIntent().getStringExtra("UserCode");
                        String mobileNo = getIntent().getStringExtra("MobileNo");

                        if(userCode.contains(" "))
                        {
                            String code[]=userCode.split(" ");
                            userCode=code[0];
                        }

                        sendSelectedGifts.setRetailerNo(userCode);
                        sendSelectedGifts.setMobileNo(mobileNo);


                        APIService service = RetroClient.getSapGiftPRetrofitService();
                        Call<JodidarandretailerGiftSelectedResult> call = null;


                        if (userType.equals("Jodidar")) {
                            call = service.insertSelectedGiftsjodidhar(sendSelectedGifts);
                        } else {

                                call = service.insertSelectedGiftsretailer(sendSelectedGifts);
                        }

                        call.enqueue(new Callback<JodidarandretailerGiftSelectedResult>() {
                            @Override
                            public void onResponse(Call<JodidarandretailerGiftSelectedResult> call, Response<JodidarandretailerGiftSelectedResult> response) {
                                try {
                                    progressDialog.dismiss();
                                    if (response.body().getResult().equals("Success")) {
                                        alertDialogpreview.dismiss();
                                        statusgiftredemmed = response.body().getData();
                                        Intent status=new Intent(getApplicationContext(), GiftRedeemedstatus.class).putExtra("list", (Serializable) statusgiftredemmed);
                                        status.setFlags(FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(status);

//                                        getalertstatus(statusgiftredemmed);

                                    } else if (response.body().getResult().equals("NotSuccess")) {
                                        alertbox.showAnimateAlertbox("No Record Found");
                                    } else {
                                        alertbox.showAnimateAlertbox("No FeedBackData Found Please contact admin");
                                    }
                                } catch (Exception e) {
                                    progressDialog.dismiss();
                                    alertDialogpreview.dismiss();
                                    alertbox.showAlertbox(getString(R.string.server_error));
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<JodidarandretailerGiftSelectedResult> call, Throwable t) {
                                progressDialog.dismiss();
                                alertDialogpreview.dismiss();
                                alertbox.showAlertbox(getString(R.string.server_error));
                            }
                        });

                    }
                });
                alertDialogpreview.show();
                alertDialogpreview.setCanceledOnTouchOutside(false);
            }
        });
    }

    private void getalertstatus(List<JodhiaretalierData> statusgiftredemmed) {

        final ProgressDialog progressDialog = new ProgressDialog(RedeemPoints.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final AlertDialog alertDialog;
        alertDialog = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = ((Activity) this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.giftstatusalert, null);
        alertDialog.getWindow().getAttributes().windowAnimations=R.style.DialogAnimations;
        alertDialog.setView(dialogView);
        progressDialog.dismiss();
        ListView listgiftresponse;
        Button okgift;


        listgiftresponse=dialogView.findViewById(R.id.listgiftresponse);
        okgift=dialogView.findViewById(R.id.okgift);
//        listgiftresponse.setAdapter(new GiftStatusAdapter(RedeemPoints.this,statusgiftredemmed));

        okgift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent home=new Intent(getApplicationContext(),Home_Activity.class);
                home.setFlags(FLAG_ACTIVITY_NEW_TASK |home.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(home);

            }
        });
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
    }
}
