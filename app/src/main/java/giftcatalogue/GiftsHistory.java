package giftcatalogue;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.effect.EffectUpdateListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.ProductSearch;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.ArrayList;
import java.util.List;

import ApiInterface.CategoryAPI;
import adapter.GiftsHistoryAdapter;
import alertbox.Alertbox;
import api.models.GetGiftHistoryRetailer.GetHistoryRetailer;
import api.models.gifthistory.GetGiftHistory;
import api.models.gifthistory.GetHistoryData;
import api.models.scanhistory.totalpoints.GetRMCode;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import home.Home_Activity;
import model.giftcatalogue.gifthistory.Jodidargifthistory;
import model.giftcatalogue.redeempoints.JodidarRedeemPoints;
import model.productsearch.ProductView;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftsHistory extends AppCompatActivity {
    ListView list;

    private List<GetHistoryData> data;
    private Alertbox alertbox = new Alertbox(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gifts_history);


        ImageView back=findViewById(R.id.gift_history_back);
        ImageView home=findViewById(R.id.gift_history_home);
        list=findViewById(R.id.gift_list_details);

        data=new ArrayList<>();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GiftsHistory.this,Home_Activity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

//        if (data==0) {
//
//            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
//            builder1.setTitle("Gift History");
//            builder1.setMessage("No Gifts Redeemed Yet.");
//            builder1.setCancelable(true);
//
//            builder1.setPositiveButton(
//                    "Ok",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            Intent back = new Intent(getApplicationContext(), Home_Activity.class);
//                            startActivity(back);
//                            dialog.cancel();
//                        }
//                    });
//            AlertDialog alert11 = builder1.create();
//            alert11.show();
//
//        }
//        getGiftHistory();

//        GiftsHistoryAdapter giftsHistoryAdapter=new GiftsHistoryAdapter(GiftsHistory.this,data);
//        list.setAdapter(giftsHistoryAdapter);

        checkInternet();

    }

    private void checkInternet(){
        NetworkConnection connection=new NetworkConnection(GiftsHistory.this);
        if(connection.CheckInternet())
        {
            giftCatalogue();
        }
        else {
            Toast.makeText(GiftsHistory.this, "No Network Connection", Toast.LENGTH_SHORT).show();

        }
    }
    private void giftCatalogue()
    {
        String userType=getIntent().getStringExtra("userType");
        String userCode=getIntent().getStringExtra("userCode");
        String mobile=getIntent().getStringExtra("mobileNo");

        /**
         * Here usercode contains code plus their name, so to remove their name following logic is used
         */
        if(userCode.contains(" "))
        {
            String code[]=userCode.split(" ");
            userCode=code[0];
        }

        APIService service = RetroClient.getSapGiftPRetrofitService();
        Call<GetGiftHistory> call = null;

        final ProgressDialog progressDialog = new ProgressDialog(GiftsHistory.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        if (userType.equals("Jodidar")){
            call=service.mechGiftHistoryPoints(userCode,mobile);

        } else if (userType.equals("Retailer")){

            call=service.getretailergifthistory(userCode,mobile);
        }
        call.enqueue(new Callback<GetGiftHistory>() {
            @Override
            public void onResponse(Call<GetGiftHistory> call, Response<GetGiftHistory> response) {
                try {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        data=response.body().getData();
                        list.setAdapter(new GiftsHistoryAdapter(GiftsHistory.this,data));

                    } else if (response.body().getResult().equals("NotSuccess")) {
                        alertbox.showAnimateAlertbox("No Record Found");

                    } else {
                        alertbox.showAnimateAlertbox("No FeedBackData Found Please contact admin");
                    }
                }catch (Exception e)
                {
                    progressDialog.dismiss();
                    alertbox.showAlertbox(getString(R.string.server_error));
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetGiftHistory> call, Throwable t) {
                progressDialog.dismiss();
                alertbox.showAlertbox(getString(R.string.server_error));
            }
        });


    }
}
