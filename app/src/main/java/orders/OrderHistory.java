package orders;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.OrderActivity;
import com.brainmagic.lucasindianservice.demo.R;

import home.Home_Activity;

public class OrderHistory extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_orders);

        ImageView back=findViewById(R.id.compeleted_order_back);
        ImageView home=findViewById(R.id.compeleted_order_home);
        ImageView cart=findViewById(R.id.completed_cart_details);
        LinearLayout pendingOrders=findViewById(R.id.pending_orders);
        LinearLayout completedOrders=findViewById(R.id.completed_order);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OrderHistory.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OrderHistory.this, OrderActivity.class));
            }
        });

        completedOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OrderHistory.this,CompletedOrders.class));
            }
        });

        pendingOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OrderHistory.this,PendingOrders.class));
            }
        });
    }
}
