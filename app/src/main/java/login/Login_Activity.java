package login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import api.models.commen.CommenResponce;
import dlr.DLR_Registeration_Activity;
import home.Home_Activity;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.brainmagic.lucasindianservice.demo.R;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

import alertbox.Alertbox;
import api.models.checkmaster.Master;
import api.models.checkmaster.Master_data;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import network.NetworkConnection;
import registration.CheckMaster_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class Login_Activity extends AppCompatActivity {


    private MaterialEditText mPassword, mUsername;
    private Button mLogin;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private Alertbox alertbox = new Alertbox(this);
    private StyleableToasty toasty = new StyleableToasty(this);
    private ProgressDialog loading;
    private CheckBox logch;
    private String mUsertype="",usertype,mobile;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);

        // Set up the login form.
        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        editor = myshare.edit();

        mUsername = (MaterialEditText) findViewById(R.id.username);
        mPassword = (MaterialEditText) findViewById(R.id.password);
        mLogin = (Button) findViewById(R.id.login_btn);
        logch = (CheckBox) findViewById(R.id.checkBox);


        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUsername.getText().length() != 10)
                    toasty.showFailureToast("Enter vaild username");
                else if (mPassword.getText().length() == 0)
                    toasty.showFailureToast("Enter your password");
                 else
                    checkInternet();
            }
        });



        logch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    mUsertype = "End User";
                else
                    mUsertype = "";
            }
        });

    }


    private void checkInternet() {

        NetworkConnection connection = new NetworkConnection(this);
        if (connection.CheckInternet())
            checkMobileNumber();
        else alertbox.showAlertbox(getResources().getString(R.string.no_internet));
    }

    private void checkMobileNumber() {
        try {
            loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<Master> call = service.Checklogin(
                    mUsername.getText().toString(),
                    mPassword.getText().toString(),
                    myshare.getString("IMEI", ""),
                    mUsertype);
            call.enqueue(new Callback<Master>() {
                @Override
                public void onResponse(Call<Master> call, Response<Master> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                            OnCheckingSuccess(response.body().getData());
                        else if (response.body().getResult().equals("Not Approved")) {
                            alertbox.showAlertbox("Please wait for admin approval Your user id password will be sent by SMS");
                        } else if (response.body().getResult().equals("InvalidUser")) {
                            alertbox.showAlertbox("Invalid username or password");
                        } else if (response.body().getResult().equals("InvalidIMEI")) {
                            alertbox.showAlertbox("You changed the device. Please wait for admin approval");
                        } else alertbox.showAlertbox(getString(R.string.server_error));
                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }
                @Override
                public void onFailure(Call<Master> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void OnCheckingSuccess(Master_data data) {
        editor.putBoolean("IsVerifyed", true);
        editor.putBoolean("IsLogin", true);
        editor.putString("UserID", data.getId());
        editor.putString("UserCode", data.getDLR_code());
        editor.putString("UserType", data.getUserType());
        editor.putString("Mobile", data.getMobileNo());
        editor.putString("UserName", data.getName());
        editor.putString("UserAddress", data.getAddress());

        editor.commit();
        startActivity(new Intent(Login_Activity.this, Home_Activity.class)
                .putExtra("UserData", data)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));


    }


    public void Onforgot(View view) {

         ArrayList<String> usertypelistforgot;
        final AlertDialog alertDialog = new AlertDialog.Builder(
                Login_Activity.this).create();

        LayoutInflater inflater = ((Activity) Login_Activity.this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.forgotpassword, null);


        final EditText mobileforgot=dialogView.findViewById(R.id.mobileforgot);
        final MaterialSpinner usertypeforgot=dialogView.findViewById(R.id.usertypeforgot);
        Button sendpassword=dialogView.findViewById(R.id.sendpassword);
        Button cancel=dialogView.findViewById(R.id.cancel);
        usertypelistforgot = new ArrayList<>();
        usertypelistforgot.add("Select");
        usertypelistforgot.add("Jodidar");
        usertypelistforgot.add("Retailer");
        usertypelistforgot.add("MSR");
        usertypelistforgot.add("LISSO");
        usertypelistforgot.add("DLR");
        usertypelistforgot.add("Stockist");
        usertypelistforgot.add("End User");
        usertypeforgot.setItems(usertypelistforgot);

        usertypeforgot.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                usertype=item.toString();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        sendpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobile=mobileforgot.getText().toString();
                usertype=usertypeforgot.getText().toString();
                if (TextUtils.isEmpty(mobile)){
                    StyleableToast st = new StyleableToast(Login_Activity.this,
                            "Enter Mobile Number", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else if (mobile.length()<10){
                    StyleableToast st = new StyleableToast(Login_Activity.this,
                            "Enter Valid Mobile Number", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else if (usertype.equals("Select")) {
                    StyleableToast st = new StyleableToast(Login_Activity.this,
                            "Select Usertype", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else {
                    alertDialog.dismiss();
                    ForgotPass(usertype,mobile);
                }
            }
        });
//        new MaterialDialog.Builder(Login_Activity.this).title("Forgot Password")
//                .content("Your password will be sent to your SMS or Email.")
//                .autoDismiss(true)
//                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT)
//                .input("Enter Mobile number", "", new MaterialDialog.InputCallback() {
//                    @Override
//                    public void onInput(MaterialDialog dialog, CharSequence input) {
//                        // Do something
//                    }
//                })
//                .positiveText("Send").negativeText("Cancel")
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        if (dialog.getInputEditText().length() != 0) {
//                            ForgotPass(dialog.getInputEditText().getText().toString());
//                        } else {
//                            StyleableToast st = new StyleableToast(Login_Activity.this,
//                                    "Enter registered email id or Mobile number !", Toast.LENGTH_SHORT);
//                            st.setBackgroundColor(getResources().getColor(R.color.red));
//                            st.setTextColor(Color.WHITE);
//                            st.setMaxAlpha();
//                            st.show();
//                        }
//
//
//                    }
//                }).onNegative(new MaterialDialog.SingleButtonCallback() {
//            @Override
//            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                dialog.dismiss();
//            }
//        }).show();
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void ForgotPass(String usertype, String mobile) {
        try {
            loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<CommenResponce> call = service.ForgotPassword(usertype,mobile);
            call.enqueue(new Callback<CommenResponce>() {
                @Override
                public void onResponse(Call<CommenResponce> call, Response<CommenResponce> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                            alertbox.showAlertbox("Password sent to your email or SMS");
                        else if (response.body().getResult().equals("InvalidUser"))
                            alertbox.showAlertbox("Invalid user");
                        else
                            alertbox.showAlertbox(getString(R.string.error_server));

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<CommenResponce> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void OnRegistration(View view) {
        startActivity(new Intent(Login_Activity.this, CheckMaster_Activity.class));
    }
}
