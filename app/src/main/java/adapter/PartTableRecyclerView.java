package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.PartDetails;
import com.brainmagic.lucasindianservice.demo.R;
import com.brainmagic.lucasindianservice.demo.UniversalActivity;

import java.util.List;

import model.AppPartList;

public class PartTableRecyclerView extends RecyclerView.Adapter<PartTableRecyclerView.MyViewHolder> {
    private Context context;
    private  List<AppPartList> Partdetails;

    public PartTableRecyclerView(Context context, List<AppPartList> Partdetails)
    {
        this.context=context;
        this.Partdetails=Partdetails;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.part_table_recycler_view,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder viewHolder, int i) {
        viewHolder.ed_engine.setText(Partdetails.get(i).getEngine());
        viewHolder.ed_starter.setText(Partdetails.get(i).getStarterMotor());
        viewHolder.ed_wipersys.setText(Partdetails.get(i).getWipermotorSystem());
        viewHolder.ed_wipermo.setText(Partdetails.get(i).getWipermotor());
        viewHolder.ed_distib.setText(Partdetails.get(i).getDistributor());

        viewHolder.ed_alter.setText(Partdetails.get(i).getAlternator());
        viewHolder.ed_wipping.setText(Partdetails.get(i).getWipingsystem());
        viewHolder.ed_rearwiper.setText(Partdetails.get(i).getRearwiper());
        viewHolder.ed_ignitioncoil.setText(Partdetails.get(i).getIgnitionCoil());
        viewHolder.ed_blowermotor.setText(Partdetails.get(i).getBlowerMotor());
        viewHolder.ed_camscan.setText(Partdetails.get(i).getCamSensor());



        if (!viewHolder.ed_starter.getText().toString().equals("")) {

            viewHolder.ed_starter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(context, PartDetails.class);
                        i.putExtra("Partnumber", viewHolder.ed_starter.getText().toString().trim());
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        if (!viewHolder.ed_wipersys.getText().toString().equals("")) {

            viewHolder.ed_wipersys.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        Intent i = new Intent(context, PartDetails.class);
                        i.putExtra("Partnumber", viewHolder.ed_wipersys.getText().toString().trim());
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        if (!viewHolder.ed_wipermo.getText().toString().equals("")) {
            viewHolder.ed_wipermo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        Intent i = new Intent(context, PartDetails.class);
                        i.putExtra("Partnumber", viewHolder.ed_wipermo.getText().toString().trim());
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        if (!viewHolder.ed_distib.getText().toString().equals("")
                ) {
            viewHolder.ed_distib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(context, PartDetails.class);
                        i.putExtra("Partnumber", viewHolder.ed_distib.getText().toString().trim());
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        if (!viewHolder.ed_alter.getText().toString().equals("")
                ) {
            viewHolder.ed_alter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(context, PartDetails.class);
                        i.putExtra("Partnumber", viewHolder.ed_alter.getText().toString().trim());
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        if (!viewHolder.ed_wipping.getText().toString().equals("")) {
            viewHolder.ed_wipping.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(context, PartDetails.class);
                        i.putExtra("Partnumber", viewHolder.ed_wipping.getText().toString().trim());
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        if (!viewHolder.ed_rearwiper.getText().toString().equals("")) {
            viewHolder.ed_rearwiper.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(context, PartDetails.class);
                        i.putExtra("Partnumber", viewHolder.ed_rearwiper.getText().toString().trim());
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        if (!viewHolder.ed_ignitioncoil.getText().toString().equals("")) {
            viewHolder.ed_ignitioncoil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(context, PartDetails.class);
                        i.putExtra("Partnumber", viewHolder.ed_ignitioncoil.getText().toString().trim());
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        if (!viewHolder.ed_wipping.getText().toString().equals("") || !viewHolder.ed_ignitioncoil.getText().toString().equals("") || !viewHolder.ed_blowermotor.getText().toString().equals("")) {
            viewHolder.ed_blowermotor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(context, PartDetails.class);
                        i.putExtra("Partnumber", viewHolder.ed_blowermotor.getText().toString().trim());
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        if (!viewHolder.ed_camscan.getText().toString().equals("")) {
            viewHolder.ed_camscan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent i = new Intent(context, PartDetails.class);
                        i.putExtra("Partnumber", viewHolder.ed_camscan.getText().toString().trim());
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return Partdetails.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView ed_engine, ed_starter, ed_wipersys, ed_wipermo, ed_distib, ed_alter, ed_wipping, ed_rearwiper, ed_ignitioncoil, ed_blowermotor, ed_camscan;
        public MyViewHolder(View view) {
            super(view);
            ed_engine = (TextView) view.findViewById(R.id.edt_engn);
            ed_starter = (TextView) view.findViewById(R.id.stater);

            ed_wipersys = (TextView) view.findViewById(R.id.wiper_system);
            ed_wipermo = (TextView) view.findViewById(R.id.wipermotor);
            ed_distib = (TextView) view.findViewById(R.id.distributor);

            ed_alter = (TextView) view.findViewById(R.id.alter);
            ed_wipping = (TextView) view.findViewById(R.id.wipping);
            ed_rearwiper = (TextView) view.findViewById(R.id.rearwiper);
            ed_ignitioncoil = (TextView) view.findViewById(R.id.ignitioncoil);
            ed_blowermotor = (TextView) view.findViewById(R.id.blowermotor);
            ed_camscan = (TextView) view.findViewById(R.id.camscanner);



        }
    }
}
