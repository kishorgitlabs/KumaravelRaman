package adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import api.models.scan.history.HistoryData;
import api.models.scanhistory.detailpoints.GetRmDetailsPointsList;
import it.sephiroth.android.library.tooltip.Tooltip;

import static android.content.Context.MODE_PRIVATE;

public class ScanHistoryAdapter extends ArrayAdapter<GetRmDetailsPointsList> {


    private List<GetRmDetailsPointsList> data,arraylist_code,arraylist_gccode,arraylist_name,arraylist_mobile,arraylist_part,arraylist_data;
    private Context context;
    private ScanHistoryAdapter_holder historyholder;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;

    public ScanHistoryAdapter(Context context, List<GetRmDetailsPointsList> localdata) {
        super(context, R.layout.adapter_view_scanhistory, localdata);
        this.context = context;
        this.data = localdata;

        arraylist_code = new ArrayList<>(localdata);
        arraylist_gccode = new ArrayList<>(localdata);
        arraylist_name = new ArrayList<>(localdata);
        arraylist_mobile = new ArrayList<>(localdata);
        arraylist_part = new ArrayList<>(localdata);
        arraylist_data = new ArrayList<>(localdata);

      /*arraylist_code.addAll(localdata);
        arraylist_gccode.addAll(localdata);
        arraylist_name.addAll(localdata);
        arraylist_mobile.addAll(localdata);
        arraylist_part.addAll(localdata);
        arraylist_data.addAll(localdata);*/

        myshare = context.getSharedPreferences("LIS", MODE_PRIVATE);
        editor = myshare.edit();
    }

    @NonNull
    @Override
    public View getView(final int position, View v, ViewGroup parent) {
        v = null;
        if (v == null) {
            v = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.adapter_view_scanhistory, parent, false);

            historyholder = new ScanHistoryAdapter_holder();
            historyholder.mPart_number = (TextView) v.findViewById(R.id.part_number);
            historyholder.sNo = (TextView) v.findViewById(R.id.s_number);
//            historyholder.mPart_des = (TextView) v.findViewById(R.id.part_des);
            historyholder.mPart_gcode = (TextView) v.findViewById(R.id.part_gc_code);
            historyholder.mPart_points = (TextView) v.findViewById(R.id.part_scan_points);
            historyholder.mDate = (TextView) v.findViewById(R.id.date);
//            historyholder.mScannedfor_other_layout = (LinearLayout) v.findViewById(R.id.scannedfor__layout);
//            historyholder.mScanned_for = (TextView) v.findViewById(R.id.scanned_for);
//            historyholder.mScan_code = (TextView) v.findViewById(R.id.scan_code);
            historyholder.mScan_method = (TextView) v.findViewById(R.id.scan_method);
//            historyholder.mScan_msg = (TextView) v.findViewById(R.id.sap_msg);


//            historyholder.mScan_msg.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//
//                    Tooltip.TooltipView tooltipView = Tooltip.make(context,
//                            new Tooltip.Builder(101)
//                                    .anchor(v.findViewById(R.id.sap_msg), Tooltip.Gravity.BOTTOM)
//                                    .closePolicy(new Tooltip.ClosePolicy()
//                                            .insidePolicy(true, false)
//                                            .outsidePolicy(true, false), 30000)
//                                    .activateDelay(2000)
//                                    .text(data.get(position).getSapSms())
//                                    .maxWidth(500)
//                                    .withStyleId(R.style.ToolTipLayoutCustomStyle)
//                                    .withArrow(true)
//                                    .withOverlay(true)
//                                    .build()
//                    );
//                    tooltipView.setTextColor(context.getResources().getColor(R.color.white));
//                    tooltipView.show();
//
//                    return false;
//                }
//            });


//            historyholder.mPart_number.setText(data.get(position).getPartNumber());
//            historyholder.mPart_gcode.setText(data.get(position).getGenCode());
//            historyholder.mPart_points.setText(data.get(position).getPoints());
//            historyholder.mDate.setText(convertDate(data.get(position).getDate()));
//            historyholder.mScan_msg.setText(data.get(position).getSapSms());
//            historyholder.mScan_method.setText(data.get(position).getSmsFrom());

            historyholder.sNo.setText(position+1+"");
            historyholder.mPart_number.setText(data.get(position).getMaterial());
            historyholder.mPart_gcode.setText(data.get(position).getUNIQUENO());
            historyholder.mPart_points.setText(data.get(position).getPoints());
            historyholder.mDate.setText((data.get(position).getDate()));
            historyholder.mScan_method.setText(data.get(position).getModeflag());

//            if (myshare.getString("UserType", "").equals("MSR")
//                    || (myshare.getString("UserType", "").equals("LISSO"))
//                    || myshare.getString("UserType", "").equals("DLR")) {
//
//                historyholder.mScannedfor_other_layout.setVisibility(View.VISIBLE);
//                historyholder.mScanned_for.setText(data.get(position).getScannerFor());
//                historyholder.mScan_code.setText(data.get(position).getJoReCode());
//
//            } else {
//                historyholder.mScannedfor_other_layout.setVisibility(View.GONE);
//            }
            v.setTag(historyholder);
        } else {
            historyholder = (ScanHistoryAdapter_holder) v.getTag();
        }
        return v;
    }

    private String convertDate(String s) {
        try {
            DateFormat df = null;
            if (s.contains("T"))
                df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            else
                df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            Date date = df.parse(s);
            String convertedDate = new SimpleDateFormat("dd-MM-yyyy").format(date);
            System.out.println(convertedDate);
            return convertedDate;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    @Override
    public int getCount() {
        return data.size();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(arraylist_data);
        } else {
            for (int i=0;i<arraylist_code.size();i++) {
                //code
                String code = arraylist_code.get(i).getPoints().toLowerCase();
                if (code.toLowerCase(Locale.getDefault()).contains(charText.toLowerCase())) {
                    data.add(arraylist_code.get(i));
                    notifyDataSetChanged();
                }
            }
            for (int i=0;i<arraylist_gccode.size();i++) {
                //code
                String gencode = arraylist_gccode.get(i).getUNIQUENO().toLowerCase();
                if (gencode.toLowerCase(Locale.getDefault()).contains(charText.toLowerCase())) {
                    data.add(arraylist_gccode.get(i));
                    notifyDataSetChanged();
                }
            }
            for (int i=0;i<arraylist_name.size();i++) {
                //code
                String name = arraylist_name.get(i).getDate().toLowerCase();
                if (name.toLowerCase(Locale.getDefault()).contains(charText.toLowerCase())) {
                    data.add(arraylist_name.get(i));
                    notifyDataSetChanged();
                }
            }
            for (int i=0;i<arraylist_mobile.size();i++) {
                //code
                String mobile = arraylist_mobile.get(i).getModeflag().toLowerCase();
                if (mobile.toLowerCase(Locale.getDefault()).contains(charText.toLowerCase())) {
                    data.add(arraylist_mobile.get(i));
                    notifyDataSetChanged();
                }
            }
            for (int i=0;i<arraylist_mobile.size();i++) {
                //code
                String part = arraylist_part.get(i).getMaterial().toLowerCase();
                if (part.toLowerCase(Locale.getDefault()).contains(charText.toLowerCase())) {
                    data.add(arraylist_part.get(i));
                    notifyDataSetChanged();
                }
            }
            Set<GetRmDetailsPointsList> removeData = new HashSet<>();
            removeData.addAll(data);
            data.clear();
            data.addAll(removeData);
        }

    }


}
 class ScanHistoryAdapter_holder {
     public TextView sNo,mPart_number, mPart_des, mPart_gcode,
             mPart_points, mDate, mScanned_for, mScan_code,mScan_method,mScan_msg;
     public LinearLayout mScannedfor_other_layout;

}
