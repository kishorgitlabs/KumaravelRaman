package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import model.ModelItemList;
import model.OECustomers;
import model.OECustomersList;

public class SegmentAdpaterTwo extends ArrayAdapter {
    private Context context;
    private List<OECustomersList> list;
    private List<ModelItemList> listItems;
    String adapter="one";
    public SegmentAdpaterTwo(@NonNull Context context, List<OECustomersList> list) {
        super(context, R.layout.segment_list_two);
        this.list=list;
        this.context=context;
    }
    public SegmentAdpaterTwo(@NonNull Context context, List<ModelItemList> list,String adapter) {
        super(context, R.layout.segment_list_two);
        this.context=context;
        this.adapter=adapter;
        this.listItems=list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;
        if(convertView==null)
        {
            if(adapter.equals("one")) {
                convertView = LayoutInflater.from(context).inflate(R.layout.segment_list_two, null);
                ImageView productName = convertView.findViewById(R.id.grid_image);
                TextView productDetail = convertView.findViewById(R.id.segment_part_name);
                productName.setScaleType(ImageView.ScaleType.FIT_CENTER);
//                productName.setPadding(8, 8, 8, 8);
//                String imageUrl = "http://lucasindianjson.brainmagicllc.com/ImageUpload/Oems/" + list.get(position).getOemImage();
                String imageUrl = list.get(position).getOemImage();
                Picasso.with(context).load(imageUrl).into(productName);
                productDetail.setText(list.get(position).getOemCustomer());
            }
            else
            {
                convertView = LayoutInflater.from(context).inflate(R.layout.segment_list_two, null);
                ImageView productName = convertView.findViewById(R.id.grid_image);
                TextView productDetail = convertView.findViewById(R.id.segment_part_name);
                productName.setScaleType(ImageView.ScaleType.FIT_CENTER);
//                productName.setPadding(8, 8, 8, 8);
                String imageUrl = "http://lucasindianjson.brainmagicllc.com/ImageUpload/App_search/" + listItems.get(position).getImage();
                Picasso.with(context).load(imageUrl).into(productName);
                productDetail.setText(listItems.get(position).getModel());
            }
        }
        return convertView;
    }

    @Override
    public int getCount() {
        if(adapter.equals("one"))
            return list.size();
        else
            return listItems.size();

    }
}
