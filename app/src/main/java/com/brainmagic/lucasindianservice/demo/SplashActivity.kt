package com.brainmagic.lucasindianservice.demo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import home.Home_Activity
import login.Login_Activity
import registration.CheckMaster_Activity
import android.content.pm.PackageManager
import android.content.pm.PackageInfo
import android.R.attr.versionCode
import android.util.Log


class SplashActivity : AppCompatActivity() {

    private var splash: ImageView? = null
    private val SPLASH_DISPLAY_LENGTH: Long = 2000
    internal lateinit var myshare: SharedPreferences
    internal lateinit var editor: SharedPreferences.Editor
    private var isVerify: Boolean = false
    private var islogin: Boolean = false
    internal var UserType: String? = null
    private var isChangePass = false




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        myshare = getSharedPreferences("LIS", Context.MODE_PRIVATE)
        editor = myshare.edit()
        splash = findViewById(R.id.imageView) as ImageView
        islogin = myshare.getBoolean("IsLogin", false)
        isVerify = myshare.getBoolean("IsVerifyed", false)
        isChangePass = myshare.getBoolean("IsChangePass", false)
        UserType = myshare.getString("logintype", "")





        Handler().postDelayed({
            if (isVerify) {

                    if (islogin) {
                        val mainIntent = Intent(this@SplashActivity, Home_Activity::class.java)
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(mainIntent)
                    } else {
                        val mainIntent = Intent(this@SplashActivity, Login_Activity::class.java)
                        startActivity(mainIntent)
                        finish()
                    }

            } else {

                val mainIntent = Intent(this@SplashActivity, CheckMaster_Activity::class.java)
                startActivity(mainIntent)
                finish()
            }

            finish()
        }, SPLASH_DISPLAY_LENGTH)
    }
}
