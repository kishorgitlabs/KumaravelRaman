package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.UniversalAdapter;
import home.Home_Activity;
import model.Universal.UniversalData;
import model.Universal.UniversalList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

public class UniversalActivity extends AppCompatActivity {

    private StyleableToasty toast;
    private ListView list;
    private ImageView product_cart_details,universal_home,universal_back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_universal);
        list=findViewById(R.id.universal_list);
        product_cart_details=findViewById(R.id.product_cart_details);
        universal_home=findViewById(R.id.universal_home);
        universal_back=findViewById(R.id.universal_back);

        NetworkConnection networkConnection=new NetworkConnection(UniversalActivity.this);
        if(networkConnection.CheckInternet())
            //get the universal data
        getUniversalDataList();
        else {
            StyleableToasty styleableToasty=new StyleableToasty(UniversalActivity.this);
            styleableToasty.showSuccessToast("Please Check Your Internet Connection");
        }
        product_cart_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UniversalActivity.this,OrderActivity.class));
            }
        });

        universal_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home=new Intent(getApplicationContext(),Home_Activity.class);
                startActivity(home);
            }
        });
        universal_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
    }


    //get the universal data as a list
    public void getUniversalDataList()
    {
        final ProgressDialog progressDialog = new ProgressDialog(UniversalActivity.this,
            R.style.Progress);
        try {
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            CategoryAPI api = RetroClient.getApiService();
            Call<UniversalList> call = api.universalList();
            toast = new StyleableToasty(UniversalActivity.this);
            call.enqueue(new Callback<UniversalList>() {
                @Override
                public void onResponse(Call<UniversalList> call, Response<UniversalList> response) {
                    if (response.body().getResult().equals("success")) {
                        progressDialog.dismiss();
                        List<UniversalData> universalData = response.body().getData();
//                    toast.showSuccessToast(universalData.get(0).getDescription());
                        list.setAdapter(new UniversalAdapter(UniversalActivity.this, universalData));
                    } else if (response.body().getResult().equals("notsuccess")){
                        progressDialog.dismiss();
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(UniversalActivity.this);
                        builder1.setTitle("Universal Parts");
                        builder1.setMessage("No FeedBackData Found");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                    else if(response.body().getResult().equals("error"))
                    {
                        progressDialog.dismiss();
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(UniversalActivity.this);
                        builder1.setTitle("Universal Parts");
                        builder1.setMessage("No FeedBackData Found");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
//                        toast.showSuccessToast("No FeedBackData Found");
                    }
                }

                @Override
                public void onFailure(Call<UniversalList> call, Throwable t) {
                    progressDialog.dismiss();
                    toast.showSuccessToast("Something went Wrong. Please try again later...!");
                    finish();
                }
            });
        }
        catch (Exception r)
        {
            progressDialog.dismiss();
            toast.showSuccessToast("Something went Wrong. Please try again later...!");
            finish();
        }
    }
}
