package com.brainmagic.lucasindianservice.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

import adapter.CartAdapter;
import home.Home_Activity;
import model.Order_Parts;
import model.PartModelClass;

public class AddtoCartActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ImageView back=findViewById(R.id.cart_back);
        ImageView home=findViewById(R.id.cart_home);
        ListView list=findViewById(R.id.cart_action_view);
        ArrayList<PartModelClass> listDetail=new ArrayList<>();


        ImageView cartButton=findViewById(R.id.cart_button);
        ImageView moreshopping=findViewById(R.id.moreshopping);
        //cart button
        cartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AddtoCartActivity.this,OrderActivity.class));
            }
        });
        //moreshoping go to product catalogue home page
        moreshopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddtoCartActivity.this,ProductCatalogue.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

//        Order_Parts cartModelDetails=new Order_Parts();
//        cartModelDetails.setPartNumber("012345");
//        cartModelDetails.setPartDescription("Do not Buy");
//        cartModelDetails.setPartName("Please");
//        cartModelDetails.setPartQuantity("1");
        PartModelClass modelClass= (PartModelClass) getIntent().getSerializableExtra("addtoCart");
//        listDetail.add(modelClass);

        //get the cart data and pass to cart
        CartAdapter cartAdapter=new CartAdapter(AddtoCartActivity.this,modelClass);
        list.setAdapter(cartAdapter);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AddtoCartActivity.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
    }
}
