
package model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class OECustomersList {

    @SerializedName("Oem_Customer")
    private String mOemCustomer;
    @SerializedName("Oem_Id")
    private Long mOemId;
    @SerializedName("OemImage")
    private String mOemImage;
    @SerializedName("Oem_priority")
    private Long mOemPriority;
    @SerializedName("Status")
    private Object mStatus;
    @SerializedName("vhid")
    private Long mVhid;
    @SerializedName("warid")
    private Long mWarid;

    public String getOemCustomer() {
        return mOemCustomer;
    }

    public void setOemCustomer(String oemCustomer) {
        mOemCustomer = oemCustomer;
    }

    public Long getOemId() {
        return mOemId;
    }

    public void setOemId(Long oemId) {
        mOemId = oemId;
    }

    public String getOemImage() {
        return mOemImage;
    }

    public void setOemImage(String oemImage) {
        mOemImage = oemImage;
    }

    public Long getOemPriority() {
        return mOemPriority;
    }

    public void setOemPriority(Long oemPriority) {
        mOemPriority = oemPriority;
    }

    public Object getStatus() {
        return mStatus;
    }

    public void setStatus(Object status) {
        mStatus = status;
    }

    public Long getVhid() {
        return mVhid;
    }

    public void setVhid(Long vhid) {
        mVhid = vhid;
    }

    public Long getWarid() {
        return mWarid;
    }

    public void setWarid(Long warid) {
        mWarid = warid;
    }

}
