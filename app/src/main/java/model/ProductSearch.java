
package model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProductSearch {

    @SerializedName("data")
    private List<ProductSearchList> mData;
    @SerializedName("result")
    private String mResult;

    public List<ProductSearchList> getData() {
        return mData;
    }

    public void setData(List<ProductSearchList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
