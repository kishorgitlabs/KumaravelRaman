
package model.orderhistory;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PendingOrders {

    @SerializedName("data")
    private List<PendingOrderList> mData;
    @SerializedName("result")
    private String mResult;

    public List<PendingOrderList> getData() {
        return mData;
    }

    public void setData(List<PendingOrderList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
