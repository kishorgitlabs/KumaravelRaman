
package model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AppPart {

    @SerializedName("data")
    private List<AppPartList> mData;
    @SerializedName("result")
    private String mResult;

    public List<AppPartList> getData() {
        return mData;
    }

    public void setData(List<AppPartList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
