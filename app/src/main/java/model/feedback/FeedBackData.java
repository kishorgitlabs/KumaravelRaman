
package model.feedback;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class FeedBackData {

    @SerializedName("ComplaintType")
    private String mComplaintType;
    @SerializedName("Emailid")
    private String mEmailid;
    @SerializedName("Feedback")
    private String mFeedback;
    @SerializedName("id")
    private Long mId;
    @SerializedName("InsertDate")
    private String mInsertDate;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("Name")
    private String mName;
    @SerializedName("UserType")
    private String mUserType;

    public String getComplaintType() {
        return mComplaintType;
    }

    public void setComplaintType(String complaintType) {
        mComplaintType = complaintType;
    }

    public String getEmailid() {
        return mEmailid;
    }

    public void setEmailid(String emailid) {
        mEmailid = emailid;
    }

    public String getFeedback() {
        return mFeedback;
    }

    public void setFeedback(String feedback) {
        mFeedback = feedback;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertDate() {
        return mInsertDate;
    }

    public void setInsertDate(String insertDate) {
        mInsertDate = insertDate;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

}
