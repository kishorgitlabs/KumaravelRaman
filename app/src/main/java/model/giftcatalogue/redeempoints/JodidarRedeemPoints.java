
package model.giftcatalogue.redeempoints;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class JodidarRedeemPoints {

    @SerializedName("data")
    private Data mData;
    @SerializedName("result")
    private String mResult;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
