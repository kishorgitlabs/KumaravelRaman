
package model.giftsumma;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SendSelectedGifts {

    private List<GiftCode> GiftCode;
    private String MobileNo;
    private String RetailerNo;

    public List<GiftCode> getGiftCode() {
        return GiftCode;
    }

    public void setGiftCode(List<GiftCode> giftCode) {
        GiftCode = giftCode;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getRetailerNo() {
        return RetailerNo;
    }

    public void setRetailerNo(String retailerNo) {
        RetailerNo = retailerNo;
    }

}
