package api.retrofit;



import api.models.checkmaster.Master;
import api.models.commen.CommenList;
import api.models.commen.CommenResponce;
import api.models.getpreviousorder.GetPreviousAddress;
import api.models.gifthistory.GetGiftHistory;
import api.models.partsdata.Parts;
import api.models.registration.RegisterModel;
import api.models.scan.history.ScanHistory;
import api.models.scan.history.ScanHistoryEndUser;
import api.models.scan.savelog.ScanPartData;
import api.models.scan.sentotp.SentOtp;
import api.models.scan.validateOTP.ValidateOTP;
import api.models.smslog.SmsLog;
import api.models.scanhistory.detailpoints.GetRMDetailsCode;
import api.models.scanhistory.totalpoints.GetRMCode;
import io.reactivex.Observable;
import model.getupdateappplaystore.GetUpdateStatusPlaystore;
import model.giftcatalogue.jodidargiftresponse.JodidarandretailerGiftSelectedResult;
import model.giftcatalogue.redeempoints.JodidarRedeemPoints;
import model.giftsumma.SendSelectedGifts;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by system01 on 5/11/2017.
 */

public interface APIService {

    @FormUrlEncoded
    @POST("API/Value/Mastercheck")
    Call<Master> CheckMobile(
            @Field("MobileNo") String mobileno,
            @Field("UserType") String mSelectedUsertype);

    @FormUrlEncoded
    @POST("API/Value/Register")
    Call<RegisterModel> SaveJodidarData(
            @Field("DLR_code") String DLR_code,
            @Field("Membership_no") String Membership_no,
            @Field("Name") String Name,
            @Field("MobileNo") String MobileNo,
            @Field("Email") String Email,
            @Field("UserType") String UserType,
            @Field("Address") String Address,
            @Field("IMEI_number") String IMEI_number,
            @Field("ModelName") String ModelName,
            @Field("ModelNumber") String ModelNumber,
            @Field("OTP") String OTP,
            @Field("Notification") boolean Notification);


    @FormUrlEncoded
    @POST("API/Value/Register")
    Call<RegisterModel> SaveMSRLISSOData(
            @Field("DLR_code") String DLR_code,
            @Field("SalesOfficerCode") String SalesOfficerCode,
            @Field("Name") String Name,
            @Field("MobileNo") String MobileNo,
            @Field("Email") String Email,
            @Field("UserType") String UserType,
            @Field("EmployeeId") String EmployeeId,
            @Field("IMEI_number") String IMEI_number,
            @Field("ModelName") String ModelName,
            @Field("ModelNumber") String ModelNumber,
            @Field("OTP") String OTP);

    @FormUrlEncoded
    @POST("API/Value/Register")
    Call<RegisterModel> SaveDLRData (
            @Field("DLR_code") String DLR_code,
            @Field("Name") String Name,
            @Field("MobileNo") String MobileNo,
            @Field("Email") String Email,
            @Field("UserType") String UserType,
            @Field("Designation") String Designation,
            @Field("Address") String Address,
            @Field("IMEI_number") String IMEI_number,
            @Field("ModelName") String ModelName,
            @Field("ModelNumber") String ModelNumber,
            @Field("OTP") String OTP);

    @GET("API/Value/GetDlrcode")
    Call<CommenList> GetDealerCode();


    @FormUrlEncoded
    @POST("API/Value/Register")
    Call<RegisterModel> SaveEndUserData(
            @Field("DLR_code") String DLR_code,
            @Field("Name") String Name,
            @Field("MobileNo") String MobileNo,
            @Field("Email") String Email,
            @Field("UserType") String UserType,
            @Field("Address") String Address,
            @Field("IMEI_number") String IMEI_number,
            @Field("ModelName") String ModelName,
            @Field("ModelNumber") String ModelNumber,
            @Field("OTP") String OTP);



    @FormUrlEncoded
    @POST("API/Value/login")
    Call<Master> Checklogin(
            @Field("MobileNo") String mobileno,
            @Field("Password") String Password,
            @Field("IMEI_number") String IMEI_number,
            @Field("UserType") String UserType);

    @FormUrlEncoded
    @POST("API/Value/ChangePassword")
    Call<Master> ChangePassowrd(
            @Field("MobileNo") String s,
            @Field("Password") String s1,
            @Field("NewPassword")String s2);

    @FormUrlEncoded
    @POST("API/Value/Forgot")
    Call<CommenResponce> ForgotPassword(
            @Field("UserType") String utype,
            @Field("MobileNo") String MobileNo);

    @FormUrlEncoded
    @POST("API/Value/ScanPart")
    Call<Parts> PartDetails(
            @Field("SerialNumber") String  mSerialNumber,
            @Field("MobileNo") String  MobileNo);



    @FormUrlEncoded
    @POST("API/Value/ScanSaveLog")
    Call<ScanPartData> SaveLog(
            @Field("SerialNumber") String  mSerialNumber,
            @Field("Code") String Code,
            @Field("JoReCode") String  JoReCode,
            @Field("MobileNo") String  MobileNo,
            @Field("UserType") String  UserType,
            @Field("Name") String  Name,
            @Field("SmsFrom") String  SmsFrom,
            @Field("SapSms") String  SapSms,
            @Field("ScannerFor") String  ScannerFor
    );


    @FormUrlEncoded
    @POST("API/Value/logout")
    Call<RegisterModel> logout(
            @Field("MobileNo") String  MobileNo,
            @Field("id") String  id
    );
    @FormUrlEncoded
    @POST("API/Value/scanhistory")
    Call<ScanHistory> GetScanHistory(
            @Field("Code") String  Code
    );

    @FormUrlEncoded
    @POST("API/Value/ViewScanfilter")
    Call<ScanHistory> GetScanHistoryDate(
            @Field("Code") String  Code,
            @Field("FromDate") String  FromDate,
            @Field("ToDate") String  ToDate
    );

    @FormUrlEncoded
    @POST("API/Value/SendOTP")
    Call<SentOtp> SentOTPtoJodidar(
            @Field("MechCode") String  MechCode,
            @Field("Code") String  Code,
            @Field("UserType") String  UserType,
            @Field("ScanDate") String  ScanDate
    );

    @FormUrlEncoded
    @POST("API/Value/ValidateOTP")
    Call<ValidateOTP> ValidateOTP(
            @Field("OTP") String  OTP,
            @Field("MechCode") String  MechCode,
            @Field("Code") String  Code,
            @Field("UserType") String  UserType,
            @Field("ScanDate") String  ScanDate
    );

//    @FormUrlEncoded
//    @POST("API/Value/autocode")
//    Call<CommenList> GetJRCodes(
//            @Field("UserType") String UserType
//    );
    @FormUrlEncoded
    @POST("API/Value/autocode1")
    Call<CommenList> GetJRCodes(
            @Field("UserType") String UserType
    );

    @FormUrlEncoded
    @POST("GetMechanic_LISSData")
    Call<CommenResponce> CallSAPForJododar(
            @Field("MobileNo") String  MobileNo,
            @Field("RetailerNo") String  RetailerNo,
            @Field("GCCode") String  GCCode
    );

    @FormUrlEncoded
    @POST("GetEndUserData")
    Call<ScanHistoryEndUser> CallSAPForEndUser(
            @Field("GCCode") String  GCCode
    );


    @FormUrlEncoded
    @POST("GetRetailer_LISSData")
    Call<CommenResponce> CallSAPForRetailer(
            @Field("MobileNo") String  MobileNo,
            @Field("RetailerNo") String  RetailerNo,
            @Field("GCCode") String  GCCode
    );

    @FormUrlEncoded
    @POST("GetLISSData")
    Call<CommenResponce> CallSAPForOthers(
            @Field("MobileNo") String  MobileNo,
            @Field("RetailerNo") String  RetailerNo,
            @Field("GCCode") String  GCCode
    );

    @FormUrlEncoded
    @POST("API/Value/Smshistory")
    Call<SmsLog> SaveSMSLog(
            @Field("MobileNo") String  MobileNo,
            @Field("Usertype") String  Usertype,
            @Field("SendFrom") String  SendFrom,
            @Field("Sms") String  Sms,
            @Field("Status") String  Status,
            @Field("Name") String  Name
    );

    @FormUrlEncoded
    @POST("GetMechanic_TotalPointDetails")
    Call<GetRMDetailsCode> detailScanHistoryForMech(
            @Field("MobileNo") String mobileno,
            @Field("RetailerNo") String mRetailerNo,
            @Field("FDate") String mFDate,
            @Field("TDate") String mTDate);

    @FormUrlEncoded
    @POST("GetRetailer_TotalPointDetails")
    Call<GetRMDetailsCode> detailScanHistoryForRetailer(
            @Field("MobileNo") String mobileno,
            @Field("RetailerNo") String mRetailerNo,
            @Field("FDate") String mFDate,
            @Field("TDate") String mTDate);

    @FormUrlEncoded
    @POST("GetMechanic_TotalPoint")
    Call<GetRMCode> totalScanHistoryForMech(
            @Field("MobileNo") String mobileno,
            @Field("RetailerNo") String mRetailerNo);

    @FormUrlEncoded
    @POST("GetRetailer_TotalPoint")
    Call<GetRMCode> totalScanHistoryForRetailer(
            @Field("MobileNo") String mobileno,
            @Field("RetailerNo") String mRetailerNo);

    @FormUrlEncoded
    @POST("GetLISS_TotalPoint ")
    Call<GetRMCode> totalScanHistoryForStockist(
            @Field("MobileNo") String mobileno,
            @Field("RetailerNo") String mRetailerNo);


    @FormUrlEncoded
    @POST("GetLISS_TotalPointDetails")
    Call<GetRMDetailsCode> detailScanHistoryForStockist(
            @Field("MobileNo") String mobileno,
            @Field("RetailerNo") String mRetailerNo,
            @Field("FDate") String mFDate,
            @Field("TDate") String mTDate);



    @FormUrlEncoded
    @POST("GetMechanic_redeemTotalPoint")
    Observable<JodidarRedeemPoints> jodidarRedeemPoints(
                @Field("MobileNo") String mobileNo,
            @Field("RetailerNo") String retailerNo
    );

    @FormUrlEncoded
    @POST("GetRetailer_redeemTotalPoint")
    Observable<JodidarRedeemPoints> retailerRedeemPoints(
            @Field("MobileNo") String mobileNo,
            @Field("RetailerNo") String retailerNo
    );

    @FormUrlEncoded
    @POST("GetMechgiftredeemDetails")
    Call<GetGiftHistory> mechGiftHistoryPoints (

            @Field("RetailerNo") String retailerNo,
            @Field("MobileNo") String mobileNo
    );

    @FormUrlEncoded
    @POST("GetRetailgiftredeemDetails")
    Call<GetGiftHistory> getretailergifthistory (

            @Field("RetailerNo") String retailerNo,
            @Field("MobileNo") String mobileNo
    );
//    );


    @POST("insrtgiftMechcode")
    Call<JodidarandretailerGiftSelectedResult> insertSelectedGiftsjodidhar (
            @Body  SendSelectedGifts giftCodeList
    );


    @POST("insrtgiftretailcode")
    Call<JodidarandretailerGiftSelectedResult> insertSelectedGiftsretailer (
            @Body  SendSelectedGifts giftCodeList
    );


    @GET("API/Value/CheckVersion")
    Call<GetUpdateStatusPlaystore> getupdate(
            @Query("version") String vcode
    );



}
