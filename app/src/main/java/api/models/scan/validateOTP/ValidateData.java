package api.models.scan.validateOTP;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class ValidateData {
  @SerializedName("Updatedate")
  @Expose
  private Object Updatedate;
  @SerializedName("MechCode")
  @Expose
  private String MechCode;
  @SerializedName("ScanDate")
  @Expose
  private String ScanDate;
  @SerializedName("Insertdate")
  @Expose
  private String Insertdate;
  @SerializedName("ValidStatus")
  @Expose
  private Boolean ValidStatus;
  @SerializedName("OTP")
  @Expose
  private String OTP;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("Code")
  @Expose
  private String Code;
  @SerializedName("UserType")
  @Expose
  private String UserType;
  public void setUpdatedate(Object Updatedate){
   this.Updatedate=Updatedate;
  }
  public Object getUpdatedate(){
   return Updatedate;
  }
  public void setMechCode(String MechCode){
   this.MechCode=MechCode;
  }
  public String getMechCode(){
   return MechCode;
  }
  public void setScanDate(String ScanDate){
   this.ScanDate=ScanDate;
  }
  public String getScanDate(){
   return ScanDate;
  }
  public void setInsertdate(String Insertdate){
   this.Insertdate=Insertdate;
  }
  public String getInsertdate(){
   return Insertdate;
  }
  public void setValidStatus(Boolean ValidStatus){
   this.ValidStatus=ValidStatus;
  }
  public Boolean getValidStatus(){
   return ValidStatus;
  }
  public void setOTP(String OTP){
   this.OTP=OTP;
  }
  public String getOTP(){
   return OTP;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setCode(String Code){
   this.Code=Code;
  }
  public String getCode(){
   return Code;
  }
  public void setUserType(String UserType){
   this.UserType=UserType;
  }
  public String getUserType(){
   return UserType;
  }
}