package api.models.scan.sentotp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class SentOtp{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private OTPData data;
  @SerializedName("mechname")
  @Expose
  private String mechname;
  @SerializedName("mechmble")
  @Expose
  private String mechmble;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(OTPData data){
   this.data=data;
  }
  public OTPData getData(){
   return data;
  }
  public void setMechname(String mechname){
   this.mechname=mechname;
  }
  public String getMechname(){
   return mechname;
  }
  public void setMechmble(String mechmble){
   this.mechmble=mechmble;
  }
  public String getMechmble(){
   return mechmble;
  }
}