
package api.models.GetGiftHistoryRetailer;

import java.util.List;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class GetHistoryRetailer {

    @SerializedName("data")
    private List<RetailerGiftData> mData;
    @SerializedName("result")
    private String mResult;

    public List<RetailerGiftData> getData() {
        return mData;
    }

    public void setData(List<RetailerGiftData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
